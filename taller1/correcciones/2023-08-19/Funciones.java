
package aed;

class Funciones {
    int cuadrado(int x) { 
        return (x*x);
    }

    double distancia(double x, double y) {
        return Math.sqrt((x*x)+(y*y));
    }

    boolean esPar(int n) {
        if (n % 2 == 0) {
            return true;
        }
        return false;
    }
    boolean esPar2(int n) {
        return n % 2 == 0;
    }

    boolean esBisiesto(int n) {
        if (n % 4 == 0 && n % 100 != 0){
            return true;
        }
        if (n % 4 == 0 && n % 400 == 0){
        return true;
        }
        return false;
    }
    
    boolean esBisiesto2 (int n) {
        return n % 4 == 0;
    } 

    int factorialIterativo(int n) {
        int i = 1;
        int res = 1;
        while(i<=n){
            res = res * i;
            i = i+1;

        }
        return res;
    }

    int factorialRecursivo(int n) {
        if (n == 0) {
            return 1;
        }

        return factorialRecursivo(n-1) * n;
    }

    boolean esPrimo(int n) {
        if (n == 1 || n == 0){
            return false;
        }
        for (int i=2; i<n; i++){
            if (n % i == 0){
                return false;
            }   
        }
        return true;
    }

    int sumatoria(int[] numeros) {
        int res = 0;
        for (int i=0; i<numeros.length ; i++){
            res = res + numeros[i] ;
        }
        return res;
    }

    int busqueda(int[] numeros, int buscado) {
        int algunaPos = 0;
        for (int i = 0 ; i<numeros.length ; i++){
            if (numeros[i]==buscado){
                algunaPos = i;
            }

        }
        return algunaPos;
    }

    boolean tienePrimo(int[] numeros) {
        for(int i = 0;i<numeros.length;i++){
            if (esPrimo(numeros[i])){
                return true;
            }
        }
        return false;
    }

    boolean todosPares(int[] numeros) {
        for(int i = 0;i<numeros.length;i++){
            if(!(esPar(numeros[i]))){
                return false;
            }
        }
        return true;
    }

    boolean esPrefijo(String s1, String s2) {
        if (s1.length()>s2.length()){
            return false;
        }
        else{
            for (int i = 0;i<s1.length();i++){
                if(s1.charAt(i) != s2.charAt(i)){
                    return false;
                }

            }
        }
        return true;
    }

    boolean esSufijo(String s1, String s2) {
        if (s1.length() > s2.length()) {
            return false;
        }
    
        String s1Inv = "";
        String s2Inv = "";
    
        for (int i = s1.length() - 1; i >= 0; i--) {
            s1Inv += s1.charAt(i);
        }
    
        for (int i = s2.length() - 1; i >= 0; i--) {
            s2Inv += s2.charAt(i);
        }
    
        return esPrefijo(s1Inv, s2Inv);}
    }

    
    
    
    
    
    
    
         